require "do_math"
Mather = DoMath.new

describe "Division" do
  context "given x = 4 and y = 2" do
    it "returns two" do
      expect(Mather./(4,2)).to eql(2)
    end
  end
end

describe "Multiplication" do 
  context "given x = 3 and y = 3" do
    it "returns nine" do
       expect(Mather.*(3,3)).to eql(9)
    end
  end
end
